<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller {


	public function action_index()
	{
		$PDO =  Database::instance('alternate');

		$query = DB::select()->from('image_management')->execute($PDO);

		$view = View::factory('index');
		$view->content = $query;
		$this->response->body($view);
	}


	public function action_upload()
    {
    	
        $view = View::factory('index');
        $error_message = NULL;
        $filedata = NULL;

        
 
        if ($this->request->method() == Request::POST)
        {
        	
            if (isset($_FILES['img']))
            {
            	
            	$title = $_POST['title'];
            	$image = $_FILES['img'];

            	
                $filedata = $this->_save_image($image, $title);
            }
        }
 
        if ( ! $filedata)
        {
            $error_message = 'There was a problem while uploading the image. Make sure it is uploaded and must be JPG/PNG/GIF file.';
                $response = array(
                	'error' => $error_message
                	);
                $this->response->headers('Content-Type', 'application/json; charset=utf-8');
                $this->response->body(json_encode($response));
                return;
        }

        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
		
        $this->response->body(json_encode($filedata));
 		
    }

    
    protected function _save_image($image, $title = '')
    {
    	

        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/';
 		
        
        if ($file = Upload::save($image, NULL, $directory))
        {

            $thumbnail = $image['name'];
            // $filename = strtolower(Text::random('alnum', 20)).'.jpg';
            $filename = explode('.', $image['name'] )[0];
            $date = date('Y-m-d', time());

            Image::factory($file)
                ->save($directory.$thumbnail);

            $dat = $this->_save_image_db($title, $thumbnail, $filename, $date);
            
            // Delete the temporary file
            unlink($file);
            $ret = array('title' => $title, 'thumbnail' => $thumbnail, 'filename' => $filename, 'created_at' => $date, 'id' => $dat['id']);
            return $ret;
        }
        
 
        return FALSE;
    }

    protected function _save_image_db($title, $thumbnail, $filename, $date)
    {
        $PDO =  Database::instance('alternate');

        $query = DB::insert('image_management', array('title', 'thumbnail', 'filename', 'created_date'))
                ->values(array($title, $thumbnail,$filename, $date))->execute($PDO);

        $query = DB::select()->from('image_management')->order_by('id', 'DESC')->limit(1)->execute($PDO);
        
        return $query[0];

    }

    public function action_editupload()
    {
        $filedata = NULL;

        if ($this->request->method() == Request::POST)
        {
            $img = NULL;
            if(isset($_FILES['img']))
            {
                $img = $_FILES['img'];
            }

            // $this->response->headers('Content-Type', 'application/json; charset=utf-8');
            // $this->response->body(json_encode($img));return;

            if($img['name'])
            {
                $filedata = $this->_update_image($img, $_POST['filename'], $_POST['title'], $_POST['id']);
            }
            else
            {
                $date = date('Y-m-d', time());
                $filedata = $this->_update_image_db($_POST['id'], $_POST['title'],'', $_POST['filename'], $date);
                
            }

            if(!$filedata)
            {
                $this->response->headers('Content-Type', 'application/json; charset=utf-8');
                $this->response->body(json_encode(array("error" => "Error")));
            }

            $this->response->headers('Content-Type', 'application/json; charset=utf-8');
            $this->response->body(json_encode($filedata));
        }
 
        

    }
 

    protected function _update_image($image, $newfilename = '', $title = '', $id)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/';
        
        
        if ($file = Upload::save($image, NULL, $directory))
        {

            $thumbnail = $newfilename ? $newfilename.'.'.explode('.', $image['name'])[1] : $image['name'];
            // $filename = strtolower(Text::random('alnum', 20)).'.jpg';
            $filename = $newfilename ? $newfilename : explode('.', $image['name'] )[0];
            $date = date('Y-m-d', time());

            Image::factory($file)
                ->save($directory.$thumbnail);

            $this->_update_image_db($id, $title, $thumbnail, $filename, $date);
                

            // Delete the temporary file
            unlink($file);
            $ret = array('title' => $title, 'thumbnail' => $thumbnail, 'filename' => $filename, 'created_at' => $date);
            return $ret;
        }
        
 
        return FALSE;
    }

    

    protected function _update_image_db($id, $title, $thumbnail ='', $filename, $date)
    {
    	$PDO =  Database::instance('alternate');

        if(!$thumbnail)
        {
            $query = DB::update('image_management')->set(array('title' => $title, 'filename' => $filename, 'created_date' => $date))->where('id', '=', $id)->execute($PDO);
            return array('title' => $title, 'thumbnail' => $thumbnail, 'filename' => $filename, 'created_at' => $date);
        }
        else
        {
            $query = DB::update('image_management')->set(array('title' => $title, 'thumbnail' => $thumbnail, 'filename' => $filename, 'created_date' => $date))->where('id', '=', $id)->execute($PDO);
            
        }

    	
    }

    public function action_deleteupload()
    {
        if ($this->request->method() == Request::POST)
        {
            if(isset($_POST['id']))
            {
                $id = $_POST['id'];

                $PDO =  Database::instance('alternate');

                $query = DB::delete('image_management')->where('id', '=', $id)->execute($PDO);
                
                if($query)
                {
                    $this->response->headers('Content-Type', 'application/json; charset=utf-8');
                    $this->response->body(json_encode(array("message" => "Deleted successfully")));
                    return;
                }
                
                $this->response->headers('Content-Type', 'application/json; charset=utf-8');
                $this->response->body(json_encode(array("error" => "Yikes! Something went wrong. Try again")));
                
            }
        }

    }

} // End Welcome
