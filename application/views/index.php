<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<title>Home page</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<h1 class="heading">Image Upload Management</h1>
			</div>
			<div class="col-sm-12 col-md-12 text-right">
				<button type="button" class="btn btn-success " data-toggle="modal" data-target="#uploadImage">
  					Upload Image
				</button>
			</div>

			<div class="col-sm-12">
				<?php if(isset($uploaded_file)): ?>
					<img src="<?php echo URL::base(TRUE) .'uploads/'. $uploaded_file['thumbnail']; ?>" alt="Uploaded avatar" />
				<?php endif; ?>

				<?php if(isset($error_message)) : ?>
					<div><?php echo $error_message; ?></div>
				<?php endif; ?>
			</div>
			<div class="col-sm-12 col-md-12">
				<hr>
				<!-- uploaded content -->
				<div class="table-responsive">
					<table class="table table-bordered" id="imageContent">
						<tr>
							<th>Title</th>
							<th>Thumbnail</th>
							<th>File Name</th>
							<th>Added Date</th>
							<th>Actions</th>
						</tr>
						<?php if(isset($content)) : ?>
							<?php  foreach($content as $row) :?>
								<tr>
									<td><?php  echo $row['title'] ?></td>
									<td><img src="<?php  echo URL::base(TRUE) . 'uploads/'. $row['thumbnail']; ?>" alt="" width="100" class="img-responsive"></td>
									<td><?php echo $row['filename'];  ?></td>
									<td><?php echo date('Y-m-d',strtotime($row['created_date']));  ?></td>
									<td>
										<button data-id="<?php echo $row['id']; ?>" class="btn btn-warning btnEdit" data-toggle="modal" data-target="#edituploadImage"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
										<button class="btn btn-danger btnDelete" data-id="<?php echo $row['id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</td>
								</tr>
							<?php  endforeach; ?>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Forms -->
	<div class="modal fade" tabindex="-1" role="dialog" id="uploadImage">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Upload Image</h4>
	      </div>
	      <form id="upload-form" action="" method="post" enctype="multipart/form-data" class="form-horizontal">
	      <div class="modal-body">
			<div class="form-group">
				<label for="" class="control-label col-sm-2">Title</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="title" id="title" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Choose file</label>
            	<div class="col-sm-10">
            		<input type="file" name="img" id="imageFile" />
            	</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary" name="submit" id="btnUpload">Upload</button>
	      </div>
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" tabindex="-1" role="dialog" id="edituploadImage">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Edit Upload Image</h4>
	      </div>
	      <form id="edit-upload-form" action="" method="post" enctype="multipart/form-data" class="form-horizontal">
	      <div class="modal-body">
			<div class="form-group">
				<label for="" class="control-label col-sm-2">Title</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="title" id="etitle" />
					<input type="hidden" id="eid" name="id">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Choose file</label>
            	<div class="col-sm-10">
            		<input type="file" name="img" id="eimageFile" />
            	</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">FileName</label>
            	<div class="col-sm-10">
            		<input type="text" name="filename" id="efilename" class="form-control" />
            	</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary" name="submit" id="btnUpload">Update</button>
	      </div>
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- End of Modal Forms -->

	<!-- Load JS @ last -->
	<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>

		$(function(){
			$("#upload-form").on('submit', function(evt){
				evt.preventDefault();
				form_action = 'index.php/home/upload';
				var formData = new FormData(this);
				console.log(formData);
				$.ajax({
					url: window.location.href + form_action,
					data: formData,
					type: 'POST',
					cache: false,
					contentType: false,
					processData:false, 
					success: function(response){
						console.log(response);
						var docHTML = "<tr><td>"+ response.title +"</td><td><img src='<?php  echo URL::base(TRUE) . 'uploads/'; ?>"+ response.thumbnail +"' width='100'/></td><td>"+ response.filename +"</td><td>"+ response.created_at +"</td><td><button class='btn btn-warning btnEdit' data-toggle='modal' data-target='#edituploadImage'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button> <button class='btn btn-danger btnDelete' data-id="+ response.id +"><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";
						$("#imageContent").append(docHTML);
						$("#uploadImage").modal("hide");
					}
				});
			});

			var domTr = '';
			$(document).on("click", ".btnEdit", function(){
				domTr = $(this).parent().parent().find('td');
				var title = domTr.eq(0).text();
				var imgURL = domTr.eq(1).find('img').attr('src');
				var filename = domTr.eq(2).text();
				var id = $(this).data('id');
				$("#etitle").val(title);
				$("#efilename").val(filename);
				$("#eid").val(id);
			});

			$("#edit-upload-form").on("submit", function(evt){
				evt.preventDefault();
				form_action = 'index.php/home/editupload';
				var formData = new FormData(this);

				$.ajax({
					url: window.location.href + form_action,
					data: formData,
					type: 'POST',
					cache: false,
					contentType: false,
					processData: false,
					success: function(response){
						console.log(response);
						domTr.eq(0).text(response.title);
						if(response.thumbnail)
							domTr.eq(1).find('img').attr('src', '<?php  echo URL::base(TRUE) . 'uploads/'; ?>' + response.thumbnail);
						domTr.eq(2).text(response.filename);
						domTr.eq(3).text(response.created_at);
						$("#edituploadImage").modal("hide");
					}
				});
			});

			$(document).on("click",".btnDelete", function(){
				var id = $(this).data("id");
				domTr = $(this).parent().parent();
				form_action = 'index.php/home/deleteupload';
				if (confirm('Are you sure you want to delete this?')) {
				    // delete it
				    $.ajax({
				    	url: window.location.href + form_action,
				    	data: {id: id},
				    	type:'POST',
				    	cache: false,
				    	success: function(response){
				    		console.log(response);
				    		if(response.message)
				    		{
				    			alert(response.message);
				    			domTr.html('');
				    			return;
				    		}
				    		alert(response.error);

				    	}
				    })
				}
			})
		});
	</script>

</body>
</html>